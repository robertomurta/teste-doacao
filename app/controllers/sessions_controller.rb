class SessionsController < ApplicationController
  def create
    ong = Ong.find_by(email: params[:email])
    if ong && ong.enabled && ong.authenticate(params[:password])
      session[:ong_id] = ong.id
      redirect_to ongs_path, notice: 'Autenticado!'
    else
      if ong.enabled
        flash.now.alert = 'O e-mail ou a senha estão incorretos.'
      else
        flash.now.alert = 'Organização desabilitada.'
      end
      render :new
    end
  end

  def destroy
    session[:ong_id] = nil
    redirect_to root_url, notice: 'Sessão finalizada!'
  end
end
