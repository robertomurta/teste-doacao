class DonationsController < ApplicationController
  # GET /donations
  # GET /donations.json
  def index
    @donations = Donation.all
  end
  
  # GET /donations/1
  # GET /donations/1.json
  def show
  end

  # GET /donations/new
  def new
    @donation = Donation.new
    @ongs = Ong.where(enabled: true ).where.not(id: 1)
  end

  # POST /donations
  # POST /donations.json
  def create
    @donation = Donation.new(donation_params)
    if @donation.ong.enabled
      respond_to do |format|
        if @donation.save
          format.html { render :show, notice: 'Muito obrigado por sua contribução.' }
          format.json { render :show, status: :created, location: @donation }
        else
          format.html { render :new }
          format.json { render json: @donation.errors, status: :unprocessable_entity }
        end
      end
    else
      format.html { render :new, notice: 'Organização desabilitada.' }
      format.json { render json: @donation.errors, status: :unprocessable_entity }
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def donation_params
    params.require(:donation).permit(:ong_id, :amount, :name, :card_number, :expiration)
  end
end
