class OngsController < ApplicationController
  before_action :require_login, only: [:edit, :update, :destroy]
  before_action :set_ong, only: [:show, :edit, :update, :destroy]
  before_action :check_permission, only: [:edit, :update, :destroy]

  # GET /ongs
  # GET /ongs.json
  def index
    @ongs = Ong.all
  end

  # GET /ongs/1
  # GET /ongs/1.json
  def show
  end

  # GET /ongs/new
  def new
    @ong = Ong.new
  end

  # GET /ongs/1/edit
  def edit
  end

  # POST /ongs
  # POST /ongs.json
  def create
    @ong = Ong.new(ong_params)

    respond_to do |format|
      if @ong.save
        format.html { redirect_to @ong, notice: 'Sua organização foi cadastrada com sucesso.' }
        format.json { render :show, status: :created, location: @ong }
      else
        format.html { render :new }
        format.json { render json: @ong.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ongs/1
  # PATCH/PUT /ongs/1.json
  def update
    respond_to do |format|
      if @ong.update(ong_params)
        format.html { redirect_to @ong, notice: 'Os dados de sua organização foram atualizados.' }
        format.json { render :show, status: :ok, location: @ong }
      else
        format.html { render :edit }
        format.json { render json: @ong.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ongs/1
  # DELETE /ongs/1.json
  def destroy
    @ong.destroy
    respond_to do |format|
      format.html { redirect_to ongs_url, notice: 'Organização removida.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ong
      @ong = Ong.find(params[:id])
    end

    # Check if logged user have permission to act on current ong
    def check_permission
      unless match_login(@ong) || is_admin?
        redirect_to login_url, notice: 'Sem permissão suficiente.'
      end
    end

    # Verify input parameters to allow only those below.
    def ong_params
      params.require(:ong).permit(:name, :email, :password, :password_confirmation, :tenant_url, :enabled)
    end
end
