class ApplicationController < ActionController::Base
    private
    def logged_in?
      @current_user ||= Ong.find(session[:ong_id]) if session[:ong_id]
    end 

    # Matches logged-in user with value.
    def match_login(id)
      session[:ong_id] == id
    end

    def is_admin?
      match_login(1)
    end

    # Confirms a logged-in user.
    def require_login
      unless logged_in?
        redirect_to login_url, notice: 'É necessário realizar login para esta ação.'
      end
    end

    helper_method :logged_in?
    helper_method :is_admin?
end
