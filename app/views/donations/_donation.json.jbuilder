json.extract! donation, :id, :ong_id, :amount, :name, :card_number, :expiration, :created_at, :updated_at
json.url donation_url(donation, format: :json)