json.extract! ong, :id, :name, :email, :tenant_url, :enabled, :created_at, :updated_at
json.url ong_url(ong, format: :json)
