Rails.application.routes.draw do
  get 'donations/new'
  get 'sessions/new'
  get 'signup', to: 'ongs#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'

  resources :ongs
  resources :sessions
  resources :donations

  root to: 'donations#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
