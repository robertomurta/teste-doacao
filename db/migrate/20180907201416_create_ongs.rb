class CreateOngs < ActiveRecord::Migration[5.2]
  def change
    create_table :ongs do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.string :tenant_url
      t.boolean :enabled

      t.timestamps
    end
  end
end
