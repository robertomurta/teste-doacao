class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.string :name
      t.string :card_number
      t.date :expiration
      t.decimal :amount, precision: 19, scale: 4
      t.references :ong, foreign_key: true

      t.timestamps
    end
  end
end
